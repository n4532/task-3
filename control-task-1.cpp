#include <iostream>
#include <fstream>

using namespace std;

void initFunc(double**, int, int);
void initTeylor(double**, int, int, double);
void testsTeylor(int, int);
void getTest(double** source, double** suspect, int n, int m, double accuracy);
double taylorExp(double x, double accuracy);
double** getRateMatrix(double**, double**, int, int);
double** allocate(int n, int m);
double getMatrixNorm(double**, int, int);
double getMax(double*, int);


int main() {

	int mode;
	int n, m;

	cout << "Enter n - to set matrix width" << endl;
	cin >> n;
	system("cls");
	cout << "Enter m - to set matrix height" << endl;
	cin >> m;
	system("cls");
	cout << "Enter 1  - to show calculation error" << endl;
	cout << "Otherwise  - to exit" << endl;
	cin >> mode;
	system("cls");

	switch (mode)
	{
	case 1:
		testsTeylor(n, m);
		break;
	default:
		return 0;
	}
	
}

double** allocate(int n, int m)
{
	double** a = new double* [n];
	for (int i = 0; i < n; i++) {
		a[i] = new double[m];
	}

	return a;
}

double** getRateMatrix(double** source, double** suspect, int n, int m)
{
	double** rate = new double* [n];
	for (int i = 0; i < n; i++) {
		rate[i] = new double[m];
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			rate[i][j] = abs(source[i][j] - suspect[i][j]);
		}
	}

	return rate;
}

void initFunc(double** a, int n, int m) {

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (i != j)
			{
				a[i][j] = (exp(i + j) + pow(i + j, 2)) / pow((i + 1), 2);
			}
			else {
				a[i][j] = 0;
			}
		}
	}
}

void initTeylor(double** a, int n, int m, double accuracy) {

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			double sum = i + j;
			if (i != j)
			{
				a[i][j] = (taylorExp(sum, accuracy) + pow(sum, 2)) / pow((i + 1), 2);
			}
			else {
				a[i][j] = 0;
			}
		}
	}
}

double getMatrixNorm(double** array, int n, int m)
{
	double* sums = new double[n];

	for (int i = 0; i < n; i++)
	{
		sums[i] = 0;
		for (int j = 0; j < m; j++)
		{
			sums[i] += array[i][m];
		}
	}

	return getMax(sums, n);
}

double getMax(double* array, int m)
{
	double maxValue = array[0];

	for (int i = 0; i < m; i++)
	{
		if (maxValue < array[i]) {
			maxValue = array[i];
		}
	}

	return maxValue;
}

double taylorExp(double x, double accuracy)
{

	double sum = 1, term = 1;
	int i = 1;

	while (abs(term) > accuracy)
	{
		term *= x / i++;
		sum += term;
	}

	return sum;
}

void testsTeylor(int n, int m)
{
	double** source = allocate(n, m);
	double** suspect = allocate(n, m);

	getTest(source, suspect, n, m, 0.1);
	getTest(source, suspect, n, m, 0.001);
	getTest(source, suspect, n, m, 0.00001);
	getTest(source, suspect, n, m, 0.0000001);
}

void getTest(double** source, double** suspect, int n, int m, double accuracy)
{
	ofstream fin;
	fin.open("text.txt");

	if (!fin.is_open()) {
		cout << "Could not open the file";
		return;
	}

	initFunc(source, n, m);
	initTeylor(suspect, n, m, accuracy);

	double** rateMatrix = getRateMatrix(source, suspect, n, m);

	fin << getMatrixNorm(rateMatrix, n, m);
	fin << endl;

	fin.close();
}